export interface IBattleStore  {
    playerDataOne:IPlayer1T;
    playerDataTwo:IPlayer2T
    loading: boolean;
    winner: player
    loser:player
}

interface IRepos {
    [key: string]: string | boolean | object | number
}
export type IPlayer1T = {
    playerOneName: string | null
    playerOneImage: string | null
}
export type IPlayer2T = {
    playerTwoName: string | null
    playerTwoImage: string | null
}
export type player = {
    profile: [] | IRepos[];
    score: string | null | number;
}