import {ActionReducerMapBuilder, AnyAction, createSlice, Slice} from "@reduxjs/toolkit";
import {getRepos} from "./popular.thunk";
import {IPopularStore} from "../../types/popular.types";


const initialState: IPopularStore ={
    selectedLanguage: localStorage.getItem("language") ? localStorage.getItem("language") : 'All ',
    loading: false,
    repos: [],
    error: null
}


const popularSlice :Slice<IPopularStore, { updateLanguage: (state: IPopularStore, action: AnyAction) => void }, string> = createSlice({
    name: 'popular',
    initialState,
    reducers: {
        updateLanguage: (state:IPopularStore, action:any) => {
            state.selectedLanguage = action.payload
        },
    },
    extraReducers: (builder:ActionReducerMapBuilder<IPopularStore>):void => {
        builder.addCase(getRepos.pending, (state:IPopularStore) => {
            state.error = null;
            state.loading = true;
        });

        builder.addCase(getRepos.fulfilled,
            (state:IPopularStore, {payload}:AnyAction):void => {
                    state.loading = false;
                    state.repos = payload;
            },);

        builder.addCase(
            getRepos.rejected,
            (state:IPopularStore, {payload}:AnyAction):void => {
                state.loading  = false;
                state.error = null;
            },
        )
    },
})

export const {
    updateLanguage
} = popularSlice.actions

export default popularSlice.reducer;