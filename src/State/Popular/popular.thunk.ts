import {fetchPopularRepos} from "../../api/Api";
import {createAsyncThunk} from "@reduxjs/toolkit";
import {AppDispatch} from "../Store";



export const getRepos = createAsyncThunk(
    'popular/getRepos',
    async (selectedLanguage:string, {rejectWithValue,dispatch}):Promise<any> => {
        try {
            return await fetchPopularRepos(selectedLanguage)
        } catch (error) {
            return rejectWithValue(error)
        }
}
)