import {AnyAction, createSlice, Slice} from "@reduxjs/toolkit";
import {getWinner} from "./battle.thunk";
import {IBattleStore} from "../../types/battle.types";


const initialState :IBattleStore = {
    playerDataOne: {
        playerOneName: "",
        playerOneImage: null,
    },
    playerDataTwo: {
        playerTwoName: "",
        playerTwoImage: null
    },
    winner: {
        profile: [],
        score: null,
    },
    loser: {
        profile: [],
        score: null,
    },
    loading: true,
}


const battleSlice:Slice<IBattleStore> = createSlice({
    name: 'battle',
    initialState,
    reducers: {
        updatePlayerOne : (state: IBattleStore, action: AnyAction) => {
            state.playerDataOne = action.payload
        },
        updatePlayerTwo: (state, action: AnyAction) => {
            state.playerDataTwo = action.payload
        },
    },
    extraReducers: (builder) => {
        builder.addCase(getWinner.pending,( state : IBattleStore) => {
            state.loading = true;
        });

        builder.addCase(getWinner.fulfilled,
            (state : IBattleStore, {payload}: AnyAction):void => {
                    state.loading = false;
                    state.winner = payload[0];
                    state.loser = payload[1];

            },);

        builder.addCase(
            getWinner.rejected,
            (state : IBattleStore, {payload}: AnyAction):void => {
                state.loading  = false;
                state.winner = payload[0];
                state.loser = payload[1];
            },
        )
    },
})

export const {
    updatePlayerOne ,
    updatePlayerTwo
} = battleSlice.actions

export default battleSlice.reducer;