import {makeBattle} from "../../api/Api";
import {createAsyncThunk} from "@reduxjs/toolkit";
import {AppDispatch} from "../Store";

// export const getWinner = (playerOne, playerTwo) => (dispatch) => {
//
//
//     makeBattle([playerOne, playerTwo])
//         .then(([winner, loser]) => {
//             dispatch(updateGetWinner(winner));
//             dispatch(updateGetLoser(loser));
//         })
//         .finally(() => dispatch(updatePlayerLoad(false)))
// }
export const getWinner = createAsyncThunk(
    'battle/getWinner',
    async (players:[string | null, string | null], {rejectWithValue,dispatch}):Promise<any>=> {
        try {
            return await makeBattle(players)
        } catch (error) {
            return rejectWithValue(error)
        }
    }
)