import {NavLink} from "react-router-dom";
import '../App.css'
import React, {FC, ReactElement} from "react";
let oldNavArray: string[] = ["Home", "Popular", "Battle"];

let navArray = oldNavArray.map((el:string,index:number) => (
    <li key={index}>
    <NavLink to={el.toLowerCase()} className = { navData => navData.isActive ? 'active' : 'item' }>
        {el}
    </NavLink>
    </li>))
const Navigation:FC = ():ReactElement => {
    return  <div >
        <ul className='nav'>
            {navArray}
        </ul>
    </div>
}

export default Navigation;

