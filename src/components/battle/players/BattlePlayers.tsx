import React, {FC, ReactElement} from "react";
import {useSelector} from "react-redux";
import {Link} from "react-router-dom";
import { IPlayer1T, IPlayer2T} from "../../../types/battle.types";
import {RootState} from "../../../State/Store";

const BattlePlayers :FC = () :ReactElement => {
    const playerDataOne:IPlayer1T = useSelector((state:RootState):IPlayer1T => state.battle.playerDataOne)
    const playerDataTwo:IPlayer2T = useSelector((state:RootState):IPlayer2T => state.battle.playerDataTwo)



    return (
        <div>
            {playerDataOne.playerOneImage && playerDataTwo.playerTwoImage ?
            <Link to={{
                pathname: 'results',
                search: `?playerOneName=${playerDataOne.playerOneName}&playerTwoName=${playerDataTwo.playerTwoName}`
            }} className='button'> battle  </Link>
            :
            null}
        </div>

    )
}

export default BattlePlayers;