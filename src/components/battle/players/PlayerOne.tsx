import {updatePlayerOne} from "../../../State/Battle/battle.slice";
import {useDispatch, useSelector} from "react-redux";
import PlayerPreview from "../PlayerPreview";
import {PlayerInputOne} from "./PlayerInputOne";
import React, {FC, ReactElement} from "react";
import {AppDispatch, RootState} from "../../../State/Store";
import {IPlayer1T} from "../../../types/battle.types";




const PlayerOne:FC = () :ReactElement => {
    const dispatch = useDispatch<AppDispatch>()
    const playerDataOne:IPlayer1T = useSelector((state: RootState):IPlayer1T => state.battle.playerDataOne)


    const handleSubmitOne = (id:number|string, userName:string):any => {
        // @ts-ignore
        return dispatch(updatePlayerOne({
            playerOneName: userName,
            playerOneImage: `https://github.com/${userName}.png?size200`,

        }))
    }
    const handleReset = (id:number|string):void => {
        // @ts-ignore
        dispatch(updatePlayerOne({
            playerOneName: "",
            playerOneImage: null
        }))
    }

        return (
            <div className='row'>
                {playerDataOne.playerOneImage ?
                    <PlayerPreview
                        avatar={playerDataOne.playerOneImage}
                        username={playerDataOne.playerOneName}
                    >
                        <button className='reset' onClick={()=> {handleReset('playerOne')}}>
                            reset
                        </button>
                    </PlayerPreview> :
                    <PlayerInputOne
                        id='playerOne'
                        label='player 1'
                        onSubmit={handleSubmitOne}
                    />}
                </div>
                )
        }


export default PlayerOne;
