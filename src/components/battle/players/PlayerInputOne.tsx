import {useDispatch, useSelector} from "react-redux";
import {updatePlayerOne} from "../../../State/Battle/battle.slice";
import React, {ChangeEvent, FC, FormEvent, ReactElement} from "react";
import {AppDispatch, RootState} from "../../../State/Store";
import {IPlayer1T} from "../../../types/battle.types";

 interface IProps {
    id: string;
    label: string;
    onSubmit: (id:string, userName:any) => void
}

export const PlayerInputOne :FC<IProps>= ({id, label, onSubmit}):ReactElement => {
    const dispatch:any = useDispatch<AppDispatch>();

    const userName :IPlayer1T = useSelector((state:RootState):IPlayer1T=> state.battle.playerDataOne)
    const handleSubmit = (event: FormEvent):void => {
        event.preventDefault()
        onSubmit(id, userName)
    }


    return (<form className='column' onSubmit={handleSubmit} >
            <label className='header' htmlFor={label}>{label}</label>
            <input
                id={label}
                type='text'
                placeholder='GitHub UserName'
                autoComplete='off'
                // @ts-ignore
                value={(userName.playerOneName)}
                onChange={(event:ChangeEvent<HTMLInputElement>):void => {
                    // @ts-ignore
                    dispatch(updatePlayerOne(event.target.value));
                }}/>
            <button className='button'
                    type='submit'
            >
                Submit
            </button>
        </form>
    )
}

