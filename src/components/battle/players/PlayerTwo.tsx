import {updatePlayerTwo} from "../../../State/Battle/battle.slice";
import {useDispatch, useSelector} from "react-redux";

import {PlayerInputTwo} from "./PlayerInputTwo";
import React from "react";
import {IPlayer2T} from "../../../types/battle.types";
import {AppDispatch, RootState} from "../../../State/Store";

const PlayerTwo = () => {
    const dispatch = useDispatch<AppDispatch>()
    const playerDataTwo:IPlayer2T = useSelector((state: RootState):IPlayer2T => state.battle.playerDataTwo)



    const handleSubmitTwo = (id:string|number, userName:string):void => {
        // @ts-ignore
        dispatch(updatePlayerTwo({
            playerTwoName: userName,
            playerTwoImage: `https://github.com/${userName}.png?size200`,

        }))
    }

    const handleReset = (id:string) :void => {
        // @ts-ignore
        dispatch(updatePlayerTwo({
            playerTwoName: "",
            playerTwoImage: null
        }))
    }
    return (
        <div className='row'>
            {playerDataTwo.playerTwoImage ?
            <div className='column'>
                <img src={playerDataTwo.playerTwoImage} alt={"avatar"} className='avatar' />
                <h3>{playerDataTwo.playerTwoName}</h3>
                <button className='reset' onClick={()=> {handleReset('playerTwo')}}>
                    reset
                </button>
            </div> :
            <PlayerInputTwo
                id='playerTwo'
                label='player 2'
                onSubmit={handleSubmitTwo}
            />}
        </div>

    )
}

export default PlayerTwo;
