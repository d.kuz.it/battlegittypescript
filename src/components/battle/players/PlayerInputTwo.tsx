import {useDispatch, useSelector} from "react-redux";
import {updatePlayerTwo} from "../../../State/Battle/battle.slice";
import React, {ChangeEvent, FC, FormEvent, ReactElement} from "react";
import {AppDispatch, RootState} from "../../../State/Store";
import {IPlayer2T} from "../../../types/battle.types";
interface IProps {
    id: string;
    label: string;
    onSubmit: (id:string, userName:any) => void
}
export const PlayerInputTwo :FC<IProps> = ({id, label, onSubmit}):ReactElement => {
    const dispatch = useDispatch<AppDispatch>();
    const userName: IPlayer2T = useSelector((state:RootState):IPlayer2T => state.battle.playerDataTwo)
    const handleSubmit = (event: FormEvent):void => {
        event.preventDefault()
        onSubmit(id, userName)
    }


    return (<form className='column' onSubmit={handleSubmit} >
            <label className='header' htmlFor={label}>{label}</label>
            <input
                id={label}
                type='text'
                placeholder='GitHub UserName'
                autoComplete='off'
                // @ts-ignore
                value={userName.playerTwoName}
                onChange={(event:ChangeEvent<HTMLInputElement>) =>
                    // @ts-ignore
                    dispatch(updatePlayerTwo(event.target.value))}/>
            <button className='button'
                    type='submit'
            >
                Submit
            </button>
        </form>
    )
}

