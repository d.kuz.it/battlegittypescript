import React, {FC, ReactElement, useEffect} from "react";
import {useLocation} from "react-router-dom";
import {PlayerResult} from "./PlayerResult";
import {Loader} from "../../Loader/Loader";
import {useDispatch, useSelector} from "react-redux";
import {getWinner} from "../../State/Battle/battle.thunk";
import {AppDispatch, RootState} from "../../State/Store";
import {player} from "../../types/battle.types";


const Results :FC = ():ReactElement => {
    const dispatch = useDispatch<AppDispatch>();
    const location = useLocation();
    const loading:boolean   = useSelector((state:RootState):boolean => state.battle.loading);
    const loser:player  = useSelector((state:RootState):player => state.battle.loser);
    const winner:player  = useSelector((state:RootState):player => state.battle.winner);

    useEffect(():void=> {
        const param = new URLSearchParams(location.search);
        const playerOne:string|null = param.get('playerOneName');
        const playerTwo:string|null = param.get('playerTwoName');
        dispatch(getWinner([playerOne, playerTwo]));
    }, [dispatch, location])

    if(loading) {
        return <Loader />
    }
  return (
      <div className='row'>

          <PlayerResult
              label='Winner'
              score={winner.score}
              profile={winner.profile}
          />
          <PlayerResult
              label='Loser'
              score={loser.score}
              profile={loser.profile}
          />
      </div>
  )
}


export default Results;