import '../../App.css';
import React, {FC, ReactElement} from "react";
import PlayerOne from "./players/PlayerOne";
import PlayerTwo from "./players/PlayerTwo";
import BattlePlayers from "./players/BattlePlayers";


const Battle:FC = () :ReactElement => {

    return (
        <>
            <div className='row'>
                <PlayerOne />
                <PlayerTwo />
            </div>
            <div>
                <BattlePlayers />
            </div>
        </>

)


}

export default Battle;