import React, {FC, ReactElement} from "react";

const PlayerPreview: FC<any> = ({avatar, username, children}):ReactElement => {
  return (
      <div className='column'>
          <img src={avatar} alt={"avatar"} className='avatar' />
          <h3>{username}</h3>
          {children}
      </div>
  )
}

export default PlayerPreview