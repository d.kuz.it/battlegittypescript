import '../../App.css';
import {Link} from "react-router-dom";
import React, {FC, ReactElement} from "react";

const Home: FC = (props):ReactElement => {
    return <div className='home-container'  >
        <h1>GitHub Battle: Battle your friends ... and stuff</h1>
        <Link to='/battle' className='button'>Battle</Link>
    </div>
}

export default Home;