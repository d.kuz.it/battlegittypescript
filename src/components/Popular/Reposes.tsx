import {useSelector} from "react-redux";
import {Loader} from "../../Loader/Loader";
import {Persons} from "./Persons";
import {FC, ReactElement} from "react";
import {RootState} from "../../State/Store";
import React from "react";


export const Reposes:FC = () :ReactElement => {
    const loading:boolean = useSelector((state: RootState):boolean => state.popular.loading)

return <>
    {loading? <Loader /> : <Persons  />}
</>

}
