import {useSelector} from "react-redux";
import {RootState} from "../../State/Store";
import {IRepos, ReposT,} from "../../types/popular.types";
import React, {FC, ReactElement} from "react";

export const Persons:FC = () :ReactElement=> {
    const repos:IRepos = useSelector((state:RootState):ReposT => state.popular.repos)


    return (

        <ul className='popular-list'>
            {repos.map((repo:IRepos, index:number):ReactElement => {
                return (
                    <li key={repo.id} className='popular-item'>
                        <div className='popular-rank'>#{index + 1 }</div>
                        <ul className='space-list-items'>
                            <li>
                                <img className='avatar' src={repo.owner.avatar_url} alt='avatar'/>
                            </li>
                            <li>
                                <a href={repo.html_url} target='_blank'>{repo.name}</a>
                            </li>
                            <li>@{repo.owner.login}</li>
                            <li>{repo.stargazers_count} stars</li>
                        </ul>
                    </li>
                )
            })}

        </ul>
    )
}
