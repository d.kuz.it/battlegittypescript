import {useDispatch, useSelector} from "react-redux";
import React, {FC, ReactElement, useEffect} from "react";
import {getRepos} from "../../State/Popular/popular.thunk";
import {NavigateOptions, URLSearchParamsInit, useSearchParams} from "react-router-dom";
import {updateLanguage} from "../../State/Popular/popular.slice";
import {AppDispatch, RootState} from "../../State/Store";
let languages:string[] = ['All ','JavaScript ','Ruby ','Java ', 'Css ','Python ', 'PHP ']

export const Language :FC = () :ReactElement => {
    const dispatch = useDispatch<AppDispatch>();
    const [searchParams, setSearchParams]: [URLSearchParams, ((nextInit?: (URLSearchParamsInit | ((prev: URLSearchParams) => URLSearchParamsInit)), navigateOpts?: NavigateOptions) => void)] = useSearchParams()
    const queryParam:string| null = searchParams.get("language")
    const selectedLanguage: string | null = useSelector((state: RootState): string | null  => state.popular.selectedLanguage)

    // @ts-ignore
    localStorage.setItem("language", (queryParam));
    useEffect(():void => {
        // @ts-ignore
        dispatch (getRepos(selectedLanguage))
        // @ts-ignore
        setSearchParams({language: selectedLanguage})
    },[selectedLanguage])



    return <ul className="languages">
        {languages.map((language:string , index:number) =>(
            <li key={index}
                style={{ color: language === selectedLanguage ? '#d0021b' : '#000000' }}
                // @ts-ignore
                onClick={():Promise<any>=> dispatch(updateLanguage(language))}>
                {language}
            </li>
        ))}
    </ul>


}
