import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import {Provider} from "react-redux";
import store from "./State/Store";
const root:ReactDOM.Root = ReactDOM.createRoot(document.getElementById('root') as Element);
// @ts-ignore
root.render(
    <Provider store={store}>
        <App />
    </Provider>
);
