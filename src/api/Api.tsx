import axios from "axios";
import {IRepos} from "../types/popular.types";

const handleError = (error:string):void => {
    throw new Error(error)
}


const getProfile = (username:string) :Promise<any> => {
        return axios.get(`https://api.github.com/users/${username}`)
            .then(user =>
                user.data
            )
            .catch(handleError)
}
const getRepos = (username:string) :Promise<any>=> {
    return axios.get(`https://api.github.com/users/${username}/repos?per_page=100`)
        .then(user =>
            user.data
        )
        .catch(handleError)
}

const getStarCount = (repos:IRepos) :Promise<any>=>  {
    return repos.reduce((acc:any, repo:IRepos) => acc + repo.stargazers_count, 0)
}

const calculateScore = (profile:any, repos:IRepos) => {
    const followers :any = profile.followers;
    const totalStar:Promise<any> = getStarCount(repos)

    return followers + totalStar;
}



const getUserData = (userName:string) :Promise<any>=> {
        return Promise.all([
          getProfile(userName),
          getRepos(userName)
        ]).then(([profile, repos]) => {
            return {
                profile,
                score: calculateScore(profile,repos)
            }
        })
            .catch(handleError)
}

const sortPlayer = (players:any) => {
        return players.sort((a:any, b:any)=>  b.score - a.score)
}

export const makeBattle = (players:any) => {
    return Promise.all(players.map(getUserData))
        .then(sortPlayer)
        .catch(handleError)
}


export const fetchPopularRepos = (language:string):Promise<any> => {

   return axios.get(window.encodeURI(`https://api.github.com/search/repositories?q=stars:>1+language:${language}&sort=desc&type=Repositories`))
       .then(response => response.data.items)

       .catch(error =>{
           throw new Error(error)
       })
       }

