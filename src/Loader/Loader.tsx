import  './Loader.css'
import React, {FC, ReactElement} from "react";
export const Loader:FC = ():ReactElement => {
  return (

          <div className="loader">
              <div className="loader-inner">
                  <div className="loader-line-wrap">
                      <div className="loader-line"></div>
                  </div>
                  <div className="loader-line-wrap">
                      <div className="loader-line"></div>
                  </div>
                  <div className="loader-line-wrap">
                      <div className="loader-line"></div>
                  </div>
                  <div className="loader-line-wrap">
                      <div className="loader-line"></div>
                  </div>
                  <div className="loader-line-wrap">
                      <div className="loader-line"></div>
                  </div>
              </div>
          </div>
  )
}
